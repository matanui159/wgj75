local Enemy = require('game.Enemy')
local Animation = require('util.Animation')
local Missile = require('game.Missile')
local Particle = require('util.Particle')

local Turret = require('util.class')()

local adioExplosion

function Turret:create(map, x, y)
   self.enemy = Enemy(map, x, y, 24, 24)
   self.animIdle = Animation('asset/enemies/turret/idle')
   self.animMove = Animation('asset/enemies/turret/move')
   self.animDie = Animation('asset/particles/explosion', false, 3)
   self.reversed = false
   self.move = false
   self.timer = 0

   if not adioExplosion then
      adioExplosion = love.audio.newSource('asset/audio/explosion.wav', 'static')
   end
   self.adioExplosion = adioExplosion
end

function Turret:update(extra, player)
   self.enemy.entity.freeze = self.die
   self.enemy:update()

   local dx = player.entity.lerp.x - self.enemy.entity.lerp.x
   local dy = player.entity.lerp.y - self.enemy.entity.lerp.y
   self.move = true
   if math.sqrt(dx * dx + dy * dy) < 120 and not player.die then
      self.move = false
      self.reversed = dx > 0
   end

   if self.move then
      local dx = -1
      if self.reversed then
         dx = 1
      end
      if self.enemy.entity:moveOne(dx, 0) then
         self.reversed = not self.reversed
      end
   end

   self.animIdle:update()
   if self.move then
      self.animMove:update()
      self.timer = 0
   else
      self.animMove:restart()
      self.timer = self.timer + 1
      if self.timer >= 75 then
         local mx = self.enemy.entity.lerp.x
         local my = self.enemy.entity.lerp.y + 5
         if self.reversed then
            mx = mx + 20
         end

         table.insert(extra, Missile(self.enemy.map, mx, my, self.reversed))
         self.timer = 0
      end
   end

   if self.die then
      table.insert(extra, Particle(
         self.enemy.entity.lerp.x - 12,
         self.enemy.entity.lerp.y - 12,
         'explosion'
      ))
      self.adioExplosion:play()
      return true
   end
end

function Turret:draw(dt)
   self.enemy:draw(dt, function(x, y)
      if self.die then
         self.animDie:draw(x - 12, y - 12)
      elseif self.move then
         self.animMove:draw(x, y, self.reversed)
      else
         self.animIdle:draw(x, y, self.reversed)
      end
   end)
end

return Turret