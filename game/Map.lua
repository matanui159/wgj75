local data = [[
S                                        S
S                                        S
S                                        S
S                                        S
S                                        S
S                                        S
S                                        S
S                E      E                S
S                                        S
S                SSSSSSSS                S
S             E     P      E             S
S   E                                E   S
S         S         SS         S         S
S        SS        SSSS        SS        S
SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
]]

local tiles = {
   [' '] = {},
   S = {solid = true, sprite = 'floor'},
   P = {},
   D = {},
   T = {},
   E = {sprite = 'camera'}
}

local Map = require('util.class')()

local TILE_SIZE = 16

function Map:create()
   self.sprites = {}
   for key, tile in pairs(tiles) do
      local sprite = tile.sprite
      if sprite ~= nil then
         local file = 'asset/tiles/' .. sprite .. '.png'
         self.sprites[sprite] = love.graphics.newImage(file)
      end
   end

   self.data = {{}}
   local x = 1
   local y = 1
   for i = 1, #data do
      local tile = data:sub(i, i)
      if tile == '\n' then
         x = 1
         y = y + 1
         table.insert(self.data, {})
      else
         self.data[y][x] = tile
         x = x + 1
      end
   end
   self.tiles = tiles
end

function Map:draw()
   for y, row in ipairs(self.data) do
      for x, tile in ipairs(row) do
         local sprite = self.tiles[tile].sprite
         if sprite ~= nil then
            love.graphics.draw(self.sprites[sprite], x * TILE_SIZE, y * TILE_SIZE)
         end
      end
   end
end

function Map:getTilesByType(type)
   local tiles = {}
   for y, row in ipairs(self.data) do
      for x, tile in ipairs(row) do
         if tile == type then
            table.insert(tiles, {
               x = x * TILE_SIZE,
               y = y * TILE_SIZE,
               width = TILE_SIZE,
               height = TILE_SIZE
            })
         end
      end
   end
   return tiles
end

function Map:getTileAt(x, y)
   x = math.floor(x / TILE_SIZE)
   y = math.floor(y / TILE_SIZE)
   if y >= 1 and y <= #self.data then
      local row = self.data[y]
      if x >= 1 and x <= #row then
         return row[x]
      end
   end
   return ' '
end

function Map:getLocalSolids(x, y)
   x = math.floor(x / TILE_SIZE)
   y = math.floor(y / TILE_SIZE)
   local tiles = {}
   for xx = x, x + 2 do
      for yy = y, y + 2 do
         local tile = self:getTileAt(xx * TILE_SIZE, yy * TILE_SIZE)
         local solid = self.tiles[tile].solid
         if solid then
            table.insert(tiles, {
               x = xx * TILE_SIZE,
               y = yy * TILE_SIZE,
               width = TILE_SIZE,
               height = TILE_SIZE
            })
         end
      end
   end
   return tiles
end

return Map