local Lerp = require('util.Lerp')

local Entity = require('util.class')()

function Entity:create(map, x, y, width, height)
   self.map = map
   self.lerp = Lerp()
   self.lerp.x = x
   self.lerp.y = y
   self.width = width or 24
   self.height = height or 24
   self.dy = 0
   self.ground = false
   self.gravity = true
   self.freeze = false
end

function Entity:update()
   self.lerp:update()
   self.ground = false
   if self.gravity then
      self.dy = self.dy + 0.5
      if self:moveOne(0, self.dy) then
         if self.dy > 0 then
            self.ground = true
         end
         self.dy = 0
      end
   end
end

function Entity:draw(dt, callback)
   self.lerp:draw(dt)
   if callback then
      callback(self.lerp.x, self.lerp.y)
   end
end

function Entity:moveOne(dx, dy)
   if self.freeze then
      return false
   end

   local ex = self.lerp.x + dx
   local ey = self.lerp.y + dy
   local result = false
   for i, solid in ipairs(self.map:getLocalSolids(ex, ey)) do
      if ex + self.width > solid.x and ex < solid.x + solid.width then
         if ey + self.height > solid.y and ey < solid.y + solid.height then
            if dx > 0 then
               ex = solid.x - self.width
            elseif dx < 0 then
               ex = solid.x + solid.width
            elseif dy > 0 then
               ey = solid.y - self.height
            elseif dy < 0 then
               ey = solid.y + solid.height
            end
            result = true
         end
      end
   end
   self.lerp.x = ex
   self.lerp.y = ey
   return result
end

function Entity:move(dx, dy)
   local result = false
   if self:moveOne(dx, 0) then
      result = true
   end
   if self:moveOne(0, dy) then
      result = true
   end
   return result
end

return Entity