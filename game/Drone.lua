local Enemy = require('game.Enemy')
local Animation = require('util.Animation')

local Drone = require('util.class')()

function Drone:create(map, x, y)
   self.enemy = Enemy(map, x, y, 20, 16)
   self.animDrone = Animation('asset/enemies/drone')
   self.enemy.entity.dy = -7
end

function Drone:update()
   self.enemy:update()
   self.enemy.entity.gravity = self.die
   self.animDrone:update()
   return self.enemy.entity.ground
end

function Drone:draw(dt)
   self.enemy:draw(dt, function(x, y)
      self.animDrone:draw(x, y, false, self.die)
   end)
end

return Drone