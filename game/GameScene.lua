local config = require('conf')
local Map = require('game.Map')
local Player = require('game.Player')
local Enemy = require('game.Enemy')
local Drone = require('game.Drone')
local Turret = require('game.Turret')
local scene = require('util.scene')

local GameScene = require('util.class')()

local adioMusic

function GameScene:create()
   self.map = Map()
   self.player = Player(self.map)
   self.background = love.graphics.newImage('asset/background.png')
   self.background:setWrap('repeat')

   self.enemies = {}
   -- for i, drone in ipairs(self.map:getTilesByType('D')) do
   --    table.insert(self.enemies, Drone(self.map, drone.x, drone.y))
   -- end
   -- for i, turret in ipairs(self.map:getTilesByType('T')) do
   --    table.insert(self.enemies, Turret(self.map, turret.x, turret.y))
   -- end

   self.extra = {}
   self.level = 1
   self.score = 0
   self.font = love.graphics.newFont(16, 'mono')

   if not adioMusic then
      adioMusic = love.audio.newSource('asset/audio/music.mp3', 'stream')
   end
   adioMusic:play()
end

function GameScene:destroy()
end

function GameScene:update()
   self.player:update(self.enemies)
   for i, enemy in ipairs(self.enemies) do
      if enemy:update(self.extra, self.player) then
         table.remove(self.enemies, i)
         i = i - 1
         self.score = self.score + 1
      end
   end

   for i, extra in ipairs(self.extra) do
      if extra:update(self.extra, self.player) then
         table.remove(self.extra, i)
         i = i - 1
      end
   end

   if #self.enemies == 0 then
      for i = 1, self.level * 2 do
         table.insert(self.enemies, Turret(
            self.map,
            math.random() * 38 * 16 + 64,
            -30
         ))
      end
      self.level = self.level + 1
   end

   if self.player.die and love.keyboard.isDown('space') then
      scene.setScene(GameScene())
   end
end

function GameScene:draw(dt)
   self.player.entity:draw(dt)

   local quad = love.graphics.newQuad(
      math.floor(self.player.entity.lerp.x / 2), 0,
      config.width, config.height,
      self.background:getWidth(), self.background:getHeight()
   )
   love.graphics.draw(self.background, quad)

   love.graphics.push()
   love.graphics.translate(-self.player.entity.lerp.x, 0)
   love.graphics.translate(-self.player.entity.width / 2, 0)
   love.graphics.translate(config.width / 2, 0)

   self.map:draw(dt)
   self.player:draw(dt)
   for i, enemy in ipairs(self.enemies) do
      enemy:draw(dt)
   end
   for i, extra in ipairs(self.extra) do
      extra:draw(dt)
   end

   love.graphics.pop()

   love.graphics.setFont(self.font)
   love.graphics.setColor(0, 0, 0)
   love.graphics.print('Score: ' .. self.score, 30, 30)

   if self.player.die then
      love.graphics.printf('PRESS "SPACE" TO RESTART',
         0,
         config.height / 2,
         config.width,
         'center'
      )
   end
   love.graphics.setColor(1, 1, 1)
end

return GameScene