local Entity = require('game.Entity')
local Animation = require('util.Animation')
local Particle = require('util.Particle')

local Missile = require('util.class')()

local adioExplosion

function Missile:create(map, x, y, reverse)
   self.entity = Entity(map, x, y, 7, 7)
   self.entity.gravity = false
   self.angle = math.pi
   if reverse then
      self.angle = 0
   end

   self.anim = Animation('asset/enemies/turret/missile')
   self.life = 0

   if not adioExplosion then
      adioExplosion = love.audio.newSource('asset/audio/explosion.wav', 'static')
   end
   self.adioExplosion = adioExplosion
end

function Missile:explode(extra)
   table.insert(extra, Particle(
      self.entity.lerp.x - 20.5,
      self.entity.lerp.y - 20.5,
      'explosion'
   ))
   self.adioExplosion:play()
   return true
end

function Missile:update(extra, player)
   self.entity:update()
   self.anim:update()
   self.life = self.life + 1

   local angleDiff = math.atan2(
      player.entity.lerp.y - self.entity.lerp.y,
      player.entity.lerp.x - self.entity.lerp.x
   ) - self.angle

   while angleDiff > math.pi do
      angleDiff = angleDiff - math.pi * 2
   end
   while angleDiff < -math.pi do
      angleDiff = angleDiff + math.pi * 2
   end

   if angleDiff < 0 then
      self.angle = self.angle - 0.1
   else
      self.angle = self.angle + 0.1
   end

   if self.entity:move(
      math.cos(self.angle) * 2,
      math.sin(self.angle) * 2
   ) then
      return self:explode(extra)
   end

   local mx = self.entity.lerp.x
   local my = self.entity.lerp.y
   local mw = self.entity.width
   local mh = self.entity.height
   local px = player.entity.lerp.x
   local py = player.entity.lerp.y
   local pw = player.entity.width
   local ph = player.entity.height
   if mx + mw > px and mx < px + pw then
      if my + mh > py and my < py + ph then
         if self.life > 10 then
            player.die = true
         end
         return self:explode(extra)
      end
   end

   if self.life % 10 == 0 then
      table.insert(extra, Particle(
         self.entity.lerp.x + 1,
         self.entity.lerp.y + 1,
         'smoke'
      ))
   end
end

function Missile:draw(dt)
   self.entity:draw(dt, function(x, y)
      x = x - 1.5
      self.anim:draw(x, y, false, false, self.angle + math.pi)
   end)
end

return Missile