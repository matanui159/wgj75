local Entity = require('game.Entity')
local Animation = require('util.Animation')

local Enemy = require('util.class')()

function Enemy:create(map, x, y, width, height)
   self.map = map
   self.move = move
   self.entity = Entity(map, x, y, width, height)
end

function Enemy:update()
   self.entity:update()
end

function Enemy:draw(dt, callback)
   self.entity:draw(dt, callback)
end

return Enemy