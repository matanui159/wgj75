local config = require('conf')
local Entity = require('game.Entity')
local Animation = require('util.Animation')

local Player = require('util.class')()

function Player:create(map)
   self.map = map
   local player = map:getTilesByType('P')[1]
   self.entity = Entity(map, player.x, player.y, 20, 25)
   self.mouse = false
   self.dx = 0
   self.reverse = false

   self.animIdle = Animation('asset/player/idle')
   self.animRun = Animation('asset/player/run')
   self.animJump = Animation('asset/player/jump', false)
   self.animFall = Animation('asset/player/fall', false)
   self.animTeleport = Animation('asset/player/teleport', false, 2)
   self.dieVelocity = -7
   self.dieOffset = 0

   self.adioJump = love.audio.newSource('asset/audio/jump.wav', 'static')
   self.adioJump:setVolume(0.5)
   self.adioZap = love.audio.newSource('asset/audio/zap.wav', 'static')
end

function Player:createZap(x1, y1, x2, y2)
   local zap = {}
   for i = 0, 1, 0.2 do
      local x = x1 * (1 - i) + x2 * i
      local y = y1 * (1 - i) + y2 * i
      if i > 0 and i < 1 then
         x = x + math.random() * 10 - 5
         y = y + math.random() * 10 - 5
      end
      table.insert(zap, x)
      table.insert(zap, y)
   end

   self.zap = {
      line = zap,
      time = 0,
      animation = true,
      teleport = {
         x = x2 - self.entity.width / 2,
         y = y2 - self.entity.height / 2
      }
   }
end

function Player:maybeZap(mx, my)
   local dx = self.entity.lerp.x - mx
   local dy = self.entity.lerp.y - my
   if math.sqrt(dx * dx + dy * dy) < 90 then
      self:createZap(
         self.entity.lerp.x + self.entity.width / 2,
         self.entity.lerp.y + self.entity.height / 2,
         mx,
         my
      )
      self.animTeleport:restart()
      self.adioZap:play()
      return true
   end
   return false
end

function Player:update(enemies)
   if self.die then
      self.entity:update()
      config.timeScale = 1
      self.dieVelocity = self.dieVelocity + 0.5
      self.dieOffset = self.dieOffset + self.dieVelocity
      return
   end

   config.timeScale = 1
   if self.zap then
      if self.zap.animation then
         self.entity.freeze = true
         if self.animTeleport:update() then
            self.entity.lerp.x = self.zap.teleport.x
            self.entity.lerp.y = self.zap.teleport.y
            self.entity.dy = 0
            self.zap.animation = false

            if self.zap.enemy then
               self.zap.enemy.die = true
            end
         end
      else
         self.entity.freeze = false
         self.zap.time = self.zap.time + 1
         config.timeScale = 0.2
         if self.zap.time > 10 then
            self.zap = nil
         end
      end
   end

   self.entity:update()

   local dx = 0
   if love.keyboard.isDown('a') then
      dx = dx - 3
   end
   if love.keyboard.isDown('d') then
      dx = dx + 3
   end
   if dx == 0 then
      self.animIdle:update()
      self.animRun:restart()
   else
      self.entity:moveOne(dx, 0)
      self.animRun:update()
      self.reverse = dx < 0
   end
   self.dx = dx

   if love.keyboard.isDown('w') and self.entity.ground then
      self.entity.dy = -7
      self.adioJump:play()
   end

   local mouse = love.mouse.isDown(1)
   if mouse and not self.mouse then
      local mx = love.mouse.getX() / config.scale
      local my = love.mouse.getY() / config.scale
      mx = mx + self.entity.lerp.x
      mx = mx + self.entity.width / 2
      mx = mx - config.width / 2

      for i, enemy in ipairs(enemies) do
         local ex = enemy.enemy.entity.lerp.x
         local ey = enemy.enemy.entity.lerp.y
         local width = enemy.enemy.entity.width
         local height = enemy.enemy.entity.height
         if mx > ex and mx < ex + width then
            if my > ey and my < ey + height then
               if self:maybeZap(mx, my) then
                  self.zap.enemy = enemy
               end
            end
         end
      end

      if self.map:getTileAt(mx, my) == 'E' then
         self:maybeZap(mx, my)
      end
   end
   self.mouse = mouse
end

function Player:draw(dt)
   self.entity:draw(dt, function(x, y)
      x = x - 2
      y = y - 7 + self.dieOffset

      if self.die then
         self.animFall:draw(x, y, self.reverse)
         return
      end

      if self.zap then
         if self.zap.animation then
            self.animTeleport:draw(x, y, self.reverse)
            return
         else
            love.graphics.setLineJoin('bevel')
            love.graphics.setLineWidth(1)
            love.graphics.setLineStyle('rough')
            love.graphics.line(unpack(self.zap.line))
            love.graphics.setLineWidth(3)
            love.graphics.setLineStyle('smooth')
            love.graphics.setColor(1, 1, 1, 0.3)
            love.graphics.line(unpack(self.zap.line))
            love.graphics.setColor(1, 1, 1)
         end
      end

      if self.entity.ground then
         if self.dx == 0 then
            self.animIdle:draw(x, y, self.reverse)
         else
            self.animRun:draw(x, y, self.reverse)
         end
      else
         if self.entity.dy < 0 then
            self.animJump:draw(x, y, self.reverse)
         else
            self.animFall:draw(x, y, self.reverse)
         end
      end
   end)
end

return Player