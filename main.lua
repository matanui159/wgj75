local config = require('conf')
local scene = require('util.scene')
local GameScene = require('game.GameScene')

local timer = 0
local canvas

function love.load()
   love.graphics.setDefaultFilter('nearest', 'nearest')
   canvas = love.graphics.newCanvas(config.width, config.height)
   scene.setScene(GameScene())
end

function love.quit()
   scene.getScene():destroy()
end

function love.update(dt)
   timer = timer + dt / 0.02 * config.timeScale
   if timer >= 1 then
      scene.getScene():update()
      timer = math.min(timer - 1, 1)
   end
end

function love.draw()
   love.graphics.setCanvas(canvas)
   love.graphics.clear()
   scene.getScene():draw(timer, config.width, config.height)
   love.graphics.setCanvas()
   love.graphics.draw(canvas, 0, 0, 0, config.scale)
end