local function class()
   local cls = {}
   setmetatable(cls, cls)
   function cls:__call(...)
      local obj = {}
      setmetatable(obj, obj)
      obj.__index = self
      obj:create(...)
      return obj
   end
   return cls
end

return class