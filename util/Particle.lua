local Animation = require('util.Animation')

local Particle = require('util.class')()

function Particle:create(x, y, name)
   self.x = x
   self.y = y
   self.anim = Animation('asset/particles/' .. name, false)
end

function Particle:update()
   return self.anim:update()
end

function Particle:draw()
   self.anim:draw(self.x, self.y)
end

return Particle