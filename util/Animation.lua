local Animation = require('util.class')()

function Animation:create(name, loop, timing)
   self.frames = {}
   local count = 0
   local result
   while true do
      local path = name .. '/' .. count .. '.png'
      local image
      result, image = pcall(love.graphics.newImage, path)
      if result then
         table.insert(self.frames, image)
         count = count + 1
      else
         break
      end
   end
   self.index = 1
   self.subIndex = 0
   
   if loop == nil then
      self.loop = true
   else
      self.loop = loop
   end

   self.timing = timing or 5
end

function Animation:update()
   local result = false
   self.subIndex = self.subIndex + 1
   if self.subIndex == self.timing then
      self.index = self.index + 1
      if self.index > #self.frames then
         if self.loop then
            self.index = 1
         else
            self.index = #self.frames
         end
         result = true
      end
      self.subIndex = 0
   end
   return result
end

function Animation:draw(x, y, flipX, flipY, angle)
   local width = self.frames[self.index]:getWidth()
   local height = self.frames[self.index]:getHeight()
   local scaleX = 1
   local scaleY = 1
   if flipX then
      scaleX = -1
   end
   if flipY then
      scaleY = -1
   end
   love.graphics.draw(
      self.frames[self.index],
      x + width / 2, y + height / 2, angle,
      scaleX, scaleY,
      width / 2, height / 2
   )
end

function Animation:restart()
   self.index = 1
   self.subIndex = 0
end

return Animation