local Lerp = require('util.class')()

function Lerp:create()
   self._lerp = {}
   self._dt = 1

   function self:__index(key)
      local value = Lerp[key]
      if value == nil then
         value = self:index(key)
      end
      return value
   end

   function self:__newindex(key, value)
      self:newIndex(key, value)
   end
end

function Lerp:update()
   for key, lerp in pairs(self._lerp) do
      lerp.prev = lerp.next
   end
   self._dt = 1
end

function Lerp:draw(dt)
   self._dt = dt
end

function Lerp:index(key)
   local lerp = self._lerp[key]
   return lerp.prev * (1 - self._dt) + lerp.next * self._dt
end

function Lerp:newIndex(key, value)
   local lerp = self._lerp[key]
   if lerp == nil then
      lerp = {
         prev = value
      }
      self._lerp[key] = lerp
   end
   lerp.next = value
end

return Lerp