local scene = {}

function scene.setScene(s)
   if scene.current ~= nil then
      scene.current:destroy()
   end
   scene.current = s
end

function scene.getScene()
   return scene.current
end

return scene