local config = {}
config.width = 480
config.height = 270
config.scale = 3
config.timeScale = 1

function love.conf(conf)
   conf.window.width = config.width * config.scale
   conf.window.height = config.height * config.scale
   conf.window.title = 'Experiment #256'
end

return config